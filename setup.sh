#!/bin/bash

#################################################################################
# Copyright (c) 2018-2021, Texas Instruments Incorporated - http://www.ti.com
# All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#################################################################################

######################################################################
version_match=`python -c 'import sys;r=0 if sys.version_info >= (3,7) and sys.version_info < (3,9) else 1;print(r)'`
if [ $version_match -ne 0 ]; then
echo 'python version must be 3.7'
exit 1
fi

######################################################################
# Installing dependencies
echo 'Installing python packages...'
#while read req; do echo ---------- $req ----------; conda install --yes $req || pip install $req; done < requirements.txt
pip install -r requirements.txt
# conda install --yes --file requirements_conda.txt



######################################################################
#NOTE: THIS STEP INSTALLS THE EDITABLE LOCAL MODULE pytorch-jacinto-ai
#NOTE: THIS IS THE MOST IMPORTANT STEP WITHOUT WHICH NONE OF THE SCRIPTS WILL WORK
echo 'Installing pytorch-jacinto-ai as a local module using setup.py'
pip install -e ./





