import os
import numpy as np
import cv2
import torch
from .. import layers as xtensor_layers
from .image_utils import *

##################################################
# a utility function used for argument parsing
def str2bool(v):
  if isinstance(v, (str)):
      if v.lower() in ("yes", "true", "t", "1"):
          return True
      elif v.lower() in ("no", "false", "f", "0"):
          return False
      else:
          return v
      #
  else:
      return v

def splitstr2bool(v):
  v = v.split(',')
  for index, args in enumerate(v):
      v[index] = str2bool(args)
  return v


#########################################################################
def make_divisible(value, factor, min_value=None):
    """
    Inspired by https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    """
    min_value = factor if min_value is None else min_value
    round_factor = factor/2
    quotient = int(value + round_factor) // factor
    value_multiple = max(quotient * factor, min_value)
    # make sure that the change is contained
    if value_multiple < 0.9*value:
        value_multiple = value_multiple + factor
    #
    return int(value_multiple)


def make_divisible_by8(v):
    return make_divisible(v, 8)


#########################################################################
def recursive_glob(rootdir='.', suffix=''):
    """Performs recursive glob with given suffix and rootdir
        :param rootdir is the root directory
        :param suffix is the suffix to be searched
    """
    return [os.path.join(looproot, filename)
            for looproot, _, filenames in os.walk(rootdir)
            for filename in filenames if filename.endswith(suffix)]


###############################################################
def get_shape_with_stride(in_shape, stride):
    shape_s = [in_shape[0],in_shape[1],in_shape[2]//stride,in_shape[3]//stride]
    if (int(in_shape[2]) % 2) == 1:
        shape_s[2] += 1
    if (int(in_shape[3]) % 2) == 1:
        shape_s[3] += 1
    return shape_s


def get_blob_from_list(x_list, search_shape, start_dim=None):
    x_ret = None
    start_dim = start_dim if start_dim is not None else 0
    for x in x_list:
        if isinstance(x, list):
            x = torch.cat(x,dim=1)
        #
        if (x.shape[start_dim:] == torch.Size(search_shape[start_dim:])):
            x_ret = x
        #
    return x_ret


